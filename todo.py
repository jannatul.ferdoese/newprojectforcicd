import json


def load_tasks():
    try:
        with open('tasks.json', 'r') as file:
            tasks = json.load(file)
    except FileNotFoundError:
        tasks = []
    return tasks


def save_tasks(tasks):
    with open('tasks.json', 'w') as file:
        json.dump(tasks, file)


def add_task(tasks):
    task_name = input("Enter the task name: ")
    task = {"name": task_name, "completed": False}
    tasks.append(task)
    save_tasks(tasks)
    print("Task added successfully.")


def complete_task(tasks):
    task_index = int(input("Enter the task index to mark as completed: "))
    if task_index < 0 or task_index >= len(tasks):
        print("Invalid task index.")
        return
    tasks[task_index]["completed"] = True
    save_tasks(tasks)
    print("Task marked as completed.")


def list_tasks(tasks):
    print("Tasks:")
    for i, task in enumerate(tasks):
        status = "[X]" if task["completed"] else "[ ]"
        print(f"{i}. {status} {task['name']}")


def main():
    tasks = load_tasks()
    while True:
        print("\n== TO-DO LIST ==")
        print("1. Add Task")
        print("2. Complete Task")
        print("3. List Tasks")
        print("4. Exit")
        choice = input("Enter your choice (1-4): ")

        if choice == "1":
            add_task(tasks)
        elif choice == "2":
            complete_task(tasks)
        elif choice == "3":
            list_tasks(tasks)
        elif choice == "4":
            print("Exiting...")
            break
        else:
            print("Invalid choice. Please try again.")


if __name__ == "__main__":
    main()
